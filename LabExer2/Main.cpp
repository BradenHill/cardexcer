#include <iostream>
#include <conio.h>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>

using namespace std; 

enum Suits
{
	CLUBS = 1, DIAMONDS = 2, HEARTS = 3, SPADES = 4
};

enum Rank
{
	ACE = 14, KING = 13, QUEEN = 12, JACK = 11, TEN = 10, NINE = 9, EIGHT = 8, SEVEN = 7, SIX = 6, FIVE = 5, FOUR = 4, THREE = 3, TWO = 2
};



struct Card
{
	Suits suit;
	Rank rank;
};

void PrintCard(Card card)
{
	cout << "The" << card.rank << "of" << card.suit << "\n";
}

Card HighCard(Card card1, Card card2)
{
	switch (card1.rank)
	{
		{
	case 2:cout << "Two";
		break;
		}
		{
	case 3:cout << "Three";
		break;
		}
		{
	case 4:cout << "Four";
		break;
		}
		{
	case 5:cout << "Five";
		break;
		}
		{
	case 6:cout << "Six";
		break;
		}
		{
	case 7:cout << "Seven";
		break;
		}
		{
	case 8:cout << "Eight";
		break;
		}
		{
	case 9:cout << "Nine";
		break;
		}
		{
	case 10:cout << "Ten";
		break;
		}
		{
	case 11:cout << "Jack";
		break;
		}
		{
	case 12:cout << "Queen";
		break;
		}
		{
	case 13:cout << "King";
		break;
		}
		{
	case 14:cout << "Ace";
		break;
		}
	}

	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card1.rank == card2.rank){

		cout << "The cards are the same" << "\n";
	}
	else {

		return card2;
	}

}


int main()
{
	
	char again;
	do {
		//YOU DRAW A CARD
			srand((unsigned)time(0));
			cout << ("You have drawn a card! ");
		int rank = 2 + (rand() % 13);
		int suit = 1 + (rand() % 4);
		switch (rank) {
		case 2:
			cout << ("You got the two of ");
			break;
		case 3:
			cout << ("You got the three of ");
			break;
		case 4:
			cout << ("You got the four of ");
			break;
		case 5:
			cout << ("You got the five of ");
			break;
		case 6:
			cout << ("You got the six of ");
			break;
		case 7:
			cout << ("You got the seven of ");
			break;
		case 8:
			cout << ("You got the eight of ");
			break;
		case 9:
			cout << ("You got the nine of ");
			break;
		case 10:
			cout << ("You got the ten of ");
			break;
		case 11:
			cout << ("You got the jack of ");
			break;
		case 12:
			cout << ("You got the queen of ");
			break;
		case 13:
			cout << ("You got the king of ");
			break;
		case 14:
			cout << ("You got the ace of ");
			break;
		default:
			cout << ("Error");
			break;
		}
		switch (suit)
		{
		case 1:
			cout << ("clubs. \n ");
				break;
		case 2:
			cout << ("diamonds. \n ");
				break;
		case 3:
			cout << ("hearts. \n ");
			break;
		case 4:
			cout << ("spades. \n ");
			break;
		default:
			cout << ("Error");
			break;
		}
		//COMPUTER DRAWS A CARD
		srand((unsigned)time(0));
		cout << ("The computer drew a card! ");
		int rank2 = 2 + (rand() % 12);
		int suit2 =(rand() % 3);
		switch (rank2) {
		case 2:
			cout << ("They got the two of ");
			break;
		case 3:
			cout << ("They got the three of ");
			break;
		case 4:
			cout << ("They got the four of ");
			break;
		case 5:
			cout << ("They got the five of ");
			break;
		case 6:
			cout << ("They got the six of ");
			break;
		case 7:
			cout << ("They got the seven of ");
			break;
		case 8:
			cout << ("They got the eight of ");
			break;
		case 9:
			cout << ("They got the nine of ");
			break;
		case 10:
			cout << ("They got the ten of ");
			break;
		case 11:
			cout << ("They got the jack of ");
			break;
		case 12:
			cout << ("They got the queen of ");
			break;
		case 13:
			cout << ("They got the king of ");
			break;
		case 14:
			cout << ("They got the ace of ");
			break;
		default:
			cout << ("Error");
			break;
		}
		switch (suit2)
		{
		case 0:
			cout << ("clubs. \n ");
			break;
		case 1:
			cout << ("diamonds. \n ");
			break;
		case 2:
			cout << ("hearts. \n ");
			break;
		case 3:
			cout << ("spades. \n ");
			break;
		default:
			cout << ("Error");
			break;
		}
		//DETERMINES IF YOU WIN OR LOSE
		if (rank > rank2)
		{
			cout << "YOU WON!!! :)  \n";
		}
		else if (rank == rank2)
		{
			cout << "IT'S A DRAW :/ \n";
		}
		else {
			cout << "YOU LOST :( \n";
		}
		cout << "Try Again? Y or N" << "\n";
		cin >> again;
	} while (again == 'y' || again == 'Y');

	_getch();
	return 0;
}